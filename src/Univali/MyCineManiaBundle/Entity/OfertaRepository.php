<?php

namespace Univali\MyCineManiaBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * OfertaRepository
 *
 */
class OfertaRepository extends EntityRepository {

  public function findOfertasDestaque() {
    $query = $this->getEntityManager()->createQuery('SELECT o FROM 
      UnivaliMyCineManiaBundle:Oferta o')->setMaxResults(3);
    return $query->getResult();
  }

  public function findOfertasSubdestaque() {
    $query = $this->getEntityManager()->createQuery('SELECT o FROM 
      UnivaliMyCineManiaBundle:Oferta o')->setMaxResults(20);
    return $query->getResult();
  }
}
