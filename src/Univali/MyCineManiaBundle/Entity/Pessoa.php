<?php

namespace Univali\MyCineManiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Pessoa
 *
 * @ORM\Table(name="pessoa")
 * @ORM\Entity
 */
class Pessoa {

  /**
   * @var integer
   *
   * @ORM\Column(name="pes_id", type="bigint", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="pes_documento", type="string", length=19, nullable=false)
   */
  private $documento;

  /**
   * @var string
   *
   * @ORM\Column(name="pes_nome", type="string", length=40, nullable=false)
   */
  private $nome;

  /**
   * @var string
   *
   * @ORM\Column(name="pes_tipo", type="string", length=3, nullable=false)
   */
  private $tipo;

  /**
   * @var string
   * 
   * @ORM\OneToOne(targetEntity="Usuario", inversedBy="pessoa", cascade={"persist"})
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="pes_usu_id", referencedColumnName="usu_id", nullable=true, unique=true)
   * })
   */
  private $usuario;

  /**
   * @var string
   * 
   * @ORM\OneToMany(targetEntity="Email", mappedBy="pessoa")
   */
  private $emails;

  /**
   * @var string
   * 
   * @ORM\OneToMany(targetEntity="Endereco", mappedBy="pessoa")
   */
  private $enderecos;

  public function __construct() {
    $this->emails = new ArrayCollection();
    $this->enderecos = new ArrayCollection();
  }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set documento
     *
     * @param string $documento
     * @return Pessoa
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;
    
        return $this;
    }

    /**
     * Get documento
     *
     * @return string 
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Pessoa
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    
        return $this;
    }

    /**
     * Get nome
     *
     * @return string 
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Pessoa
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Add emails
     *
     * @param \Univali\MyCineManiaBundle\Entity\Email $emails
     * @return Pessoa
     */
    public function addEmail(\Univali\MyCineManiaBundle\Entity\Email $emails)
    {
        $this->emails[] = $emails;
    
        return $this;
    }

    /**
     * Remove emails
     *
     * @param \Univali\MyCineManiaBundle\Entity\Email $emails
     */
    public function removeEmail(\Univali\MyCineManiaBundle\Entity\Email $emails)
    {
        $this->emails->removeElement($emails);
    }

    /**
     * Get emails
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * Add enderecos
     *
     * @param \Univali\MyCineManiaBundle\Entity\Endereco $enderecos
     * @return Pessoa
     */
    public function addEndereco(\Univali\MyCineManiaBundle\Entity\Endereco $enderecos)
    {
        $this->enderecos[] = $enderecos;
    
        return $this;
    }

    /**
     * Remove enderecos
     *
     * @param \Univali\MyCineManiaBundle\Entity\Endereco $enderecos
     */
    public function removeEndereco(\Univali\MyCineManiaBundle\Entity\Endereco $enderecos)
    {
        $this->enderecos->removeElement($enderecos);
    }

    /**
     * Get enderecos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEnderecos()
    {
        return $this->enderecos;
    }

    /**
     * Get pessoa
     *
     * @return \Univali\MyCineManiaBundle\Entity\Usuario $usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set usuario
     *
     * @param \Univali\MyCineManiaBundle\Entity\Usuario $usuario
     */
    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }
}