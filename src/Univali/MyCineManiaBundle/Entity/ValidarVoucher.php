<?php

namespace Univali\MyCineManiaBundle\Entity;

/**
 * Validar Voucher
 */
class ValidarVoucher {

  /**
   * @var integer
   */
  private $codigo;

  /**
   * Get codigo
   *
   * @return integer 
   */
  public function getCodigo() {
    return $this->codigo;
  }

  /**
   * Set codigo
   */
  public function setCodigo($codigo) {
    $this->codigo = $codigo;
  }

}