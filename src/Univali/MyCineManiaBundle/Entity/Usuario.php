<?php

namespace Univali\MyCineManiaBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * 
 * 
 * @author Murilo
 * 
 * @ORM\Entity
 * @ORM\Table
 */
class Usuario extends BaseUser {

  /**
   * @ORM\Id
   * @ORM\Column(name="usu_id", type="integer")
   * @ORM\GeneratedValue(strategy="AUTO")
   */
  protected $id;

  public function __construct() {
    parent::__construct();
  }

  /**
   * @var \Pessoa
   * @ORM\OneToMany(targetEntity="Pessoa", mappedBy="usuario")
   */
  private $pessoa;

  /**
   * Get id
   *
   * @return integer 
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set pessoa
   *
   * @param \Univali\MyCineManiaBundle\Entity\Pessoa $pessoa
   * @return Usuario
   */
  public function setPessoa(\Univali\MyCineManiaBundle\Entity\Pessoa $pessoa) {
    $this->pessoa = $pessoa;

    return $this;
  }

  /**
   * Get pessoa
   *
   * @return \Univali\MyCineManiaBundle\Entity\Pessoa 
   */
  public function getPessoa() {
    return $this->pessoa;
  }

}