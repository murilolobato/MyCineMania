<?php

namespace Univali\MyCineManiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Oferta
 *
 * @ORM\Table(name="oferta")
 * @ORM\Entity(repositoryClass="Univali\MyCineManiaBundle\Entity\OfertaRepository")
 */
class Oferta {

  /**
   * @var integer
   *
   * @ORM\Column(name="ofe_id", type="bigint", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="ofe_dt_inicio", type="date", nullable=false)
   */
  private $dataInicio;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="ofe_dt_termino", type="date", nullable=false)
   */
  private $dataTermino;

  /**
   * @var string
   *
   * @ORM\Column(name="ofe_descricao", type="text", nullable=false)
   */
  private $descricao;

  /**
   * @var string
   *
   * @ORM\Column(name="ofe_regra", type="text", nullable=false)
   */
  private $regra;

  /**
   * @var string
   *
   * @ORM\Column(name="ofe_titulo", type="string", length=35, nullable=false)
   */
  private $titulo;

  /**
   * @var \Cinema
   *
   * @ORM\ManyToOne(targetEntity="Cinema", inversedBy="ofertas")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="ofe_cma_id", referencedColumnName="cma_id", nullable=false)
   * })
   */
  private $cinema;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dataInicio
     *
     * @param \DateTime $dataInicio
     * @return Oferta
     */
    public function setDataInicio($dataInicio)
    {
        $this->dataInicio = $dataInicio;
    
        return $this;
    }

    /**
     * Get dataInicio
     *
     * @return \DateTime 
     */
    public function getDataInicio()
    {
        return $this->dataInicio;
    }

    /**
     * Set dataTermino
     *
     * @param \DateTime $dataTermino
     * @return Oferta
     */
    public function setDataTermino($dataTermino)
    {
        $this->dataTermino = $dataTermino;
    
        return $this;
    }

    /**
     * Get dataTermino
     *
     * @return \DateTime 
     */
    public function getDataTermino()
    {
        return $this->dataTermino;
    }

    /**
     * Set descricao
     *
     * @param string $descricao
     * @return Oferta
     */
    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    
        return $this;
    }

    /**
     * Get descricao
     *
     * @return string 
     */
    public function getDescricao()
    {
        return $this->descricao;
    }

    /**
     * Set regra
     *
     * @param string $regra
     * @return Oferta
     */
    public function setRegra($regra)
    {
        $this->regra = $regra;
    
        return $this;
    }

    /**
     * Get regra
     *
     * @return string 
     */
    public function getRegra()
    {
        return $this->regra;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     * @return Oferta
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    
        return $this;
    }

    /**
     * Get titulo
     *
     * @return string 
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set cinema
     *
     * @param \Univali\MyCineManiaBundle\Entity\Cinema $cinema
     * @return Oferta
     */
    public function setCinema(\Univali\MyCineManiaBundle\Entity\Cinema $cinema)
    {
        $this->cinema = $cinema;
    
        return $this;
    }

    /**
     * Get cinema
     *
     * @return \Univali\MyCineManiaBundle\Entity\Cinema 
     */
    public function getCinema()
    {
        return $this->cinema;
    }
}