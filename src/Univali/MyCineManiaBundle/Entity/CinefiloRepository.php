<?php

namespace Univali\MyCineManiaBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * CinefiloRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CinefiloRepository extends EntityRepository {

  public function findByPessoaId($id) {
    $query = $this->getEntityManager()->createQuery('SELECT p, c FROM 
      UnivaliMyCineManiaBundle:Cinefilo c JOIN c.pessoa p 
      WHERE p.id = :id')->setParameter('id', $id);
    try {
      return $query->getSingleResult();
    } catch (\Doctrine\ORM\NoResultException $e) {
      return null;
    }
  }

}
