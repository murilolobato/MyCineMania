<?php

namespace Univali\MyCineManiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Endereco
 *
 * @ORM\Table(name="endereco")
 * @ORM\Entity
 */
class Endereco {

  /**
   * @var integer
   *
   * @ORM\Column(name="end_id", type="bigint", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="end_bairro", type="string", length=30, nullable=false)
   */
  private $bairro;

  /**
   * @var string
   *
   * @ORM\Column(name="end_cep", type="string", length=10, nullable=false)
   */
  private $cep;

  /**
   * @var string
   *
   * @ORM\Column(name="end_cidade", type="string", length=50, nullable=false)
   */
  private $cidade;

  /**
   * @var string
   *
   * @ORM\Column(name="end_complemento", type="string", length=30, nullable=true)
   */
  private $complemento;

  /**
   * @var string
   *
   * @ORM\Column(name="end_estado", type="string", length=2, nullable=false)
   */
  private $estado;

  /**
   * @var string
   *
   * @ORM\Column(name="end_logradouro", type="string", length=30, nullable=false)
   */
  private $logradouro;

  /**
   * @var integer
   *
   * @ORM\Column(name="end_numero", type="integer", nullable=true)
   */
  private $numero;

  /**
   * @var \Pessoa
   *
   * @ORM\ManyToOne(targetEntity="Pessoa", inversedBy="enderecos")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="end_pes_id", referencedColumnName="pes_id",nullable=false)
   * })
   */
  private $pessoa;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bairro
     *
     * @param string $bairro
     * @return Endereco
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    
        return $this;
    }

    /**
     * Get bairro
     *
     * @return string 
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * Set cep
     *
     * @param string $cep
     * @return Endereco
     */
    public function setCep($cep)
    {
        $this->cep = $cep;
    
        return $this;
    }

    /**
     * Get cep
     *
     * @return string 
     */
    public function getCep()
    {
        return $this->cep;
    }

    /**
     * Set cidade
     *
     * @param string $cidade
     * @return Endereco
     */
    public function setCidade($cidade)
    {
        $this->cidade = $cidade;
    
        return $this;
    }

    /**
     * Get cidade
     *
     * @return string 
     */
    public function getCidade()
    {
        return $this->cidade;
    }

    /**
     * Set complemento
     *
     * @param string $complemento
     * @return Endereco
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;
    
        return $this;
    }

    /**
     * Get complemento
     *
     * @return string 
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * Set estado
     *
     * @param string $estado
     * @return Endereco
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return string 
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set logradouro
     *
     * @param string $logradouro
     * @return Endereco
     */
    public function setLogradouro($logradouro)
    {
        $this->logradouro = $logradouro;
    
        return $this;
    }

    /**
     * Get logradouro
     *
     * @return string 
     */
    public function getLogradouro()
    {
        return $this->logradouro;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     * @return Endereco
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    
        return $this;
    }

    /**
     * Get numero
     *
     * @return integer 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set pessoa
     *
     * @param \Univali\MyCineManiaBundle\Entity\Pessoa $pessoa
     * @return Endereco
     */
    public function setPessoa(\Univali\MyCineManiaBundle\Entity\Pessoa $pessoa)
    {
        $this->pessoa = $pessoa;
    
        return $this;
    }

    /**
     * Get pessoa
     *
     * @return \Univali\MyCineManiaBundle\Entity\Pessoa 
     */
    public function getPessoa()
    {
        return $this->pessoa;
    }
}