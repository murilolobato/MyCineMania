<?php

namespace Univali\MyCineManiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Voucher
 *
 * @ORM\Table(name="voucher")
 * @ORM\Entity
 */
class Voucher {

  /**
   * @var integer
   *
   * @ORM\Column(name="vou_id", type="bigint", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="vou_dt_emissao", type="datetime", nullable=false)
   */
  private $dataEmissao;

  /**
   * @var string
   *
   * @ORM\Column(name="vou_situacao", type="string", length=3, nullable=false)
   */
  private $situacao;

  /**
   * @var \Oferta
   *
   * @ORM\ManyToOne(targetEntity="Oferta")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="vou_ofe_id", referencedColumnName="ofe_id", nullable=false)
   * })
   */
  private $oferta;

  /**
   * @var \Cinefilo
   *
   * @ORM\ManyToOne(targetEntity="Cinefilo", inversedBy="vouchers")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="vou_cfo_id", referencedColumnName="cfo_id", nullable=false)
   * })
   */
  private $cinefilo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dataEmissao
     *
     * @param \DateTime $dataEmissao
     * @return Voucher
     */
    public function setDataEmissao($dataEmissao)
    {
        $this->dataEmissao = $dataEmissao;
    
        return $this;
    }

    /**
     * Get dataEmissao
     *
     * @return \DateTime 
     */
    public function getDataEmissao()
    {
        return $this->dataEmissao;
    }

    /**
     * Set situacao
     *
     * @param string $situacao
     * @return Voucher
     */
    public function setSituacao($situacao)
    {
        $this->situacao = $situacao;
    
        return $this;
    }

    /**
     * Get situacao
     *
     * @return string 
     */
    public function getSituacao()
    {
        return $this->situacao;
    }

    /**
     * Set oferta
     *
     * @param \Univali\MyCineManiaBundle\Entity\Oferta $oferta
     * @return Voucher
     */
    public function setOferta(\Univali\MyCineManiaBundle\Entity\Oferta $oferta)
    {
        $this->oferta = $oferta;
    
        return $this;
    }

    /**
     * Get oferta
     *
     * @return \Univali\MyCineManiaBundle\Entity\Oferta 
     */
    public function getOferta()
    {
        return $this->oferta;
    }

    /**
     * Set cinefilo
     *
     * @param \Univali\MyCineManiaBundle\Entity\Cinefilo $cinefilo
     * @return Voucher
     */
    public function setCinefilo(\Univali\MyCineManiaBundle\Entity\Cinefilo $cinefilo)
    {
        $this->cinefilo = $cinefilo;
    
        return $this;
    }

    /**
     * Get cinefilo
     *
     * @return \Univali\MyCineManiaBundle\Entity\Cinefilo 
     */
    public function getCinefilo()
    {
        return $this->cinefilo;
    }
}