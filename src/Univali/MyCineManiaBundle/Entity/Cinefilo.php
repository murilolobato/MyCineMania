<?php

namespace Univali\MyCineManiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Cinefilo
 *
 * @ORM\Table(name="cinefilo")
 * @ORM\Entity(repositoryClass="Univali\MyCineManiaBundle\Entity\CinefiloRepository")
 */
class Cinefilo {

  /**
   * @var integer
   *
   * @ORM\Column(name="cfo_id", type="bigint", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="cfo_cpf", type="string", length=14, nullable=false)
   */
  private $cpf;

  /**
   * @var \Pessoa
   *
   * @ORM\OneToOne(targetEntity="Pessoa", cascade={"persist"})
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="cfo_pes_id", referencedColumnName="pes_id", nullable=false,unique=true)
   * })
   */
  private $pessoa;

  /**
   * @ORM\OneToMany(targetEntity="Voucher", mappedBy="cinefilo")
   */
  private $vouchers;
  
  /**
   * @ORM\OneToMany(targetEntity="Movimentacao", mappedBy="cinefilo")
   */
  private $movimentacoes;

  public function __construct() {
    $this->vouchers = new ArrayCollection();
    $this->movimentacoes = new ArrayCollection();
  }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cpf
     *
     * @param string $cpf
     * @return Cinefilo
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    
        return $this;
    }

    /**
     * Get cpf
     *
     * @return string 
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * Set pessoa
     *
     * @param \Univali\MyCineManiaBundle\Entity\Pessoa $pessoa
     * @return Cinefilo
     */
    public function setPessoa(\Univali\MyCineManiaBundle\Entity\Pessoa $pessoa)
    {
        $this->pessoa = $pessoa;
    
        return $this;
    }

    /**
     * Get pessoa
     *
     * @return \Univali\MyCineManiaBundle\Entity\Pessoa 
     */
    public function getPessoa()
    {
        return $this->pessoa;
    }

    /**
     * Add vouchers
     *
     * @param \Univali\MyCineManiaBundle\Entity\Voucher $vouchers
     * @return Cinefilo
     */
    public function addVoucher(\Univali\MyCineManiaBundle\Entity\Voucher $vouchers)
    {
        $this->vouchers[] = $vouchers;
    
        return $this;
    }

    /**
     * Remove vouchers
     *
     * @param \Univali\MyCineManiaBundle\Entity\Voucher $vouchers
     */
    public function removeVoucher(\Univali\MyCineManiaBundle\Entity\Voucher $vouchers)
    {
        $this->vouchers->removeElement($vouchers);
    }

    /**
     * Get vouchers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getVouchers()
    {
        return $this->vouchers;
    }

    /**
     * Add movimentacoes
     *
     * @param \Univali\MyCineManiaBundle\Entity\Movimentacao $movimentacoes
     * @return Cinefilo
     */
    public function addMovimentacoe(\Univali\MyCineManiaBundle\Entity\Movimentacao $movimentacoes)
    {
        $this->movimentacoes[] = $movimentacoes;
    
        return $this;
    }

    /**
     * Remove movimentacoes
     *
     * @param \Univali\MyCineManiaBundle\Entity\Movimentacao $movimentacoes
     */
    public function removeMovimentacoe(\Univali\MyCineManiaBundle\Entity\Movimentacao $movimentacoes)
    {
        $this->movimentacoes->removeElement($movimentacoes);
    }

    /**
     * Get movimentacoes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMovimentacoes()
    {
        return $this->movimentacoes;
    }
    
    public function __toString() {
      return $this->getPessoa()->getNome();
    }
}