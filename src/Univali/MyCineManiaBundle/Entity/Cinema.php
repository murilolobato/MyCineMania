<?php

namespace Univali\MyCineManiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Cinema
 *
 * @ORM\Table(name="cinema")
 * @ORM\Entity(repositoryClass="Univali\MyCineManiaBundle\Entity\CinemaRepository")
 */
class Cinema {

  /**
   * @var integer
   *
   * @ORM\Column(name="cma_id", type="bigint", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="cma_nome_fantasia", type="string", length=30, nullable=false)
   */
  private $nomeFantasia;

  /**
   * @var \Pessoa
   *
   * @ORM\OneToOne(targetEntity="Pessoa", cascade={"persist"})
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="cma_pes_id", referencedColumnName="pes_id",nullable=false,unique=true)
   * })
   */
  private $pessoa;

  /**
   * @var string
   * 
   * @ORM\OneToMany(targetEntity="Oferta", mappedBy="cinema")
   */
  private $ofertas;

  public function __construct() {
    $this->ofertas = new ArrayCollection();
  }

  /**
   * Get id
   *
   * @return integer 
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set nomeFantasia
   *
   * @param string $nomeFantasia
   * @return Cinema
   */
  public function setNomeFantasia($nomeFantasia) {
    $this->nomeFantasia = $nomeFantasia;

    return $this;
  }

  /**
   * Get nomeFantasia
   *
   * @return string 
   */
  public function getNomeFantasia() {
    return $this->nomeFantasia;
  }

  /**
   * Set pessoa
   *
   * @param \Univali\MyCineManiaBundle\Entity\Pessoa $pessoa
   * @return Cinema
   */
  public function setPessoa(\Univali\MyCineManiaBundle\Entity\Pessoa $pessoa) {
    $this->pessoa = $pessoa;

    return $this;
  }

  /**
   * Get pessoa
   *
   * @return \Univali\MyCineManiaBundle\Entity\Pessoa 
   */
  public function getPessoa() {
    return $this->pessoa;
  }

  /**
   * Add ofertas
   *
   * @param \Univali\MyCineManiaBundle\Entity\Oferta $ofertas
   * @return Cinema
   */
  public function addOferta(\Univali\MyCineManiaBundle\Entity\Oferta $ofertas) {
    $this->ofertas[] = $ofertas;

    return $this;
  }

  /**
   * Remove ofertas
   *
   * @param \Univali\MyCineManiaBundle\Entity\Oferta $ofertas
   */
  public function removeOferta(\Univali\MyCineManiaBundle\Entity\Oferta $ofertas) {
    $this->ofertas->removeElement($ofertas);
  }

  /**
   * Get ofertas
   *
   * @return \Doctrine\Common\Collections\Collection 
   */
  public function getOfertas() {
    return $this->ofertas;
  }

  public function __toString() {
    return $this->getPessoa()->getNome();
  }
}