<?php

namespace Univali\MyCineManiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Email
 *
 * @ORM\Table(name="email")
 * @ORM\Entity
 */
class Email {

  /**
   * @var integer
   *
   * @ORM\Column(name="eml_id", type="bigint", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="eml_rotulo", type="string", length=15, nullable=true)
   */
  private $rotulo;

  /**
   * @var string
   *
   * @ORM\Column(name="eml_endereco", type="string", length=100, nullable=false, unique=true)
   */
  private $endereco;

  /**
   * @var \Pessoa
   *
   * @ORM\ManyToOne(targetEntity="Pessoa", inversedBy="emails")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="eml_pes_id", referencedColumnName="pes_id",nullable=false)
   * })
   */
  private $pessoa;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rotulo
     *
     * @param string $rotulo
     * @return Email
     */
    public function setRotulo($rotulo)
    {
        $this->rotulo = $rotulo;
    
        return $this;
    }

    /**
     * Get rotulo
     *
     * @return string 
     */
    public function getRotulo()
    {
        return $this->rotulo;
    }

    /**
     * Set endereco
     *
     * @param string $endereco
     * @return Email
     */
    public function setEndereco($endereco)
    {
        $this->endereco = $endereco;
    
        return $this;
    }

    /**
     * Get endereco
     *
     * @return string 
     */
    public function getEndereco()
    {
        return $this->endereco;
    }

    /**
     * Set pessoa
     *
     * @param \Univali\MyCineManiaBundle\Entity\Pessoa $pessoa
     * @return Email
     */
    public function setPessoa(\Univali\MyCineManiaBundle\Entity\Pessoa $pessoa)
    {
        $this->pessoa = $pessoa;
    
        return $this;
    }

    /**
     * Get pessoa
     *
     * @return \Univali\MyCineManiaBundle\Entity\Pessoa 
     */
    public function getPessoa()
    {
        return $this->pessoa;
    }
}