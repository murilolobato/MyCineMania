<?php

namespace Univali\MyCineManiaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Movimentacao
 *
 * @ORM\Table(name="movimentacao")
 * @ORM\Entity
 */
class Movimentacao {

  /**
   * @var integer
   *
   * @ORM\Column(name="mov_id", type="bigint", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="mov_data", type="date", nullable=false)
   */
  private $data;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="mov_dt_cadastro", type="date", nullable=false)
   */
  private $dataCadastro;

  /**
   * @var string
   *
   * @ORM\Column(name="mov_tipo", type="string", length=3, nullable=false)
   */
  private $tipo;

  /**
   * @var \DateTime
   *
   * @ORM\Column(name="mov_dt_validade", type="date", nullable=true)
   */
  private $dataValidade;

  /**
   * @var \Cinefilo
   *
   * @ORM\ManyToOne(targetEntity="Cinefilo", inversedBy="movimentacoes")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="mov_cfo_cod", referencedColumnName="cfo_id",nullable=false)
   * })
   */
  private $cinefilo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     * @return Movimentacao
     */
    public function setData($data)
    {
        $this->data = $data;
    
        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set dataCadastro
     *
     * @param \DateTime $dataCadastro
     * @return Movimentacao
     */
    public function setDataCadastro($dataCadastro)
    {
        $this->dataCadastro = $dataCadastro;
    
        return $this;
    }

    /**
     * Get dataCadastro
     *
     * @return \DateTime 
     */
    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Movimentacao
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;
    
        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set dataValidade
     *
     * @param \DateTime $dataValidade
     * @return Movimentacao
     */
    public function setDataValidade($dataValidade)
    {
        $this->dataValidade = $dataValidade;
    
        return $this;
    }

    /**
     * Get dataValidade
     *
     * @return \DateTime 
     */
    public function getDataValidade()
    {
        return $this->dataValidade;
    }

    /**
     * Set cinefilo
     *
     * @param \Univali\MyCineManiaBundle\Entity\Cinefilo $cinefilo
     * @return Movimentacao
     */
    public function setCinefilo(\Univali\MyCineManiaBundle\Entity\Cinefilo $cinefilo)
    {
        $this->cinefilo = $cinefilo;
    
        return $this;
    }

    /**
     * Get cinefilo
     *
     * @return \Univali\MyCineManiaBundle\Entity\Cinefilo 
     */
    public function getCinefilo()
    {
        return $this->cinefilo;
    }
}