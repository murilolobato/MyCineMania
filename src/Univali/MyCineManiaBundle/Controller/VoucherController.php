<?php

namespace Univali\MyCineManiaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Univali\MyCineManiaBundle\Entity\Voucher;
use Univali\MyCineManiaBundle\Entity\ValidarVoucher;
use Univali\MyCineManiaBundle\Form\VoucherType;
use Univali\MyCineManiaBundle\Form\ValidarVoucherType;

/**
 * Voucher controller.
 *
 */
class VoucherController extends Controller {

  /**
   * Lists all Voucher entities.
   *
   */
  public function indexAction() {
    $em = $this->getDoctrine()->getManager();

    $entities = $em->getRepository('UnivaliMyCineManiaBundle:Voucher')->findAll();

    return $this->render('UnivaliMyCineManiaBundle:Voucher:index.html.twig', array(
                'entities' => $entities,
    ));
  }

  /**
   * Creates a new Voucher entity.
   *
   */
  public function createAction(Request $request) {
    $entity = new Voucher();
    $form = $this->createForm(new VoucherType(), $entity);
    $form->bind($request);

    if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($entity);
      $em->flush();

      return $this->redirect($this->generateUrl('voucher_show', array('id' => $entity->getId())));
    }

    return $this->render('UnivaliMyCineManiaBundle:Voucher:new.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
    ));
  }

  /**
   * Displays a form to create a new Voucher entity.
   *
   */
  public function newAction() {
    $entity = new Voucher();
    $form = $this->createForm(new VoucherType(), $entity);

    return $this->render('UnivaliMyCineManiaBundle:Voucher:new.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
    ));
  }

  /**
   * Finds and displays a Voucher entity.
   *
   */
  public function showAction($id) {
    $em = $this->getDoctrine()->getManager();

    $entity = $em->getRepository('UnivaliMyCineManiaBundle:Voucher')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find Voucher entity.');
    }

    $deleteForm = $this->createDeleteForm($id);

    return $this->render('UnivaliMyCineManiaBundle:Voucher:show.html.twig', array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),));
  }

  public function exibeValidacaoAction() {
    $entity = new ValidarVoucher();
    $form = $this->createForm(new ValidarVoucherType(), $entity);

    return $this->render('UnivaliMyCineManiaBundle:Voucher:validar.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
    ));
  }
  
  public function validarAction(Request $request) {
    $validarVoucher = new ValidarVoucher();
    $form = $this->createForm(new ValidarVoucherType(), $validarVoucher);
    $form->bind($request);

    $voucherExists = $em->getRepository('UnivaliMyCineManiaBundle:voucher')->findById($validarVoucher->getCodigo());
    if (sizeof($voucherExists) > 0) {
      $form->addError(new \Symfony\Component\Form\FormError("Voucher Inválido!"));
    }
    
    if ($form->hasErrors()) {
      return $this->redirect($this->generateUrl('voucher_valido'));
    } else {
      return $this->render('UnivaliMyCineManiaBundle:voucher:validar.html.twig', array(
                  'entity' => $validarVoucher,
                  'form' => $form->createView(),
      ));
    }
  }
}
