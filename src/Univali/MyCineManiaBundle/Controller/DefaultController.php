<?php

namespace Univali\MyCineManiaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller {

  public function indexAction() {

    $em = $this->getDoctrine()->getManager();
    
    $ofertasDestaque = $em->getRepository('UnivaliMyCineManiaBundle:Oferta')->findOfertasDestaque();
    
    $ofertasSubdestaque = $em->getRepository('UnivaliMyCineManiaBundle:Oferta')->findOfertasSubdestaque();
    
    $ofertas = $em->getRepository('UnivaliMyCineManiaBundle:Oferta')->findAll();

    return $this->render('UnivaliMyCineManiaBundle::index.html.twig', array(
                'ofertasDestaque' => $ofertasDestaque,
                'ofertasSubdestaque' => $ofertasSubdestaque,
                'ofertas' => $ofertas
    ));
  }

}
