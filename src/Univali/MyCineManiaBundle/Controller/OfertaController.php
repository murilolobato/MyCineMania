<?php

namespace Univali\MyCineManiaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Univali\MyCineManiaBundle\Entity\Oferta;
use Univali\MyCineManiaBundle\Form\OfertaType;

/**
 * Oferta controller.
 *
 */
class OfertaController extends Controller {

  public function listarAction() {

    $em = $this->getDoctrine()->getManager();

    $entities = $em->getRepository('UnivaliMyCineManiaBundle:Oferta')->findAll();

    return $this->render('UnivaliMyCineManiaBundle:Oferta:listar.html.twig', array(
                'ofertas' => $entities,
    ));
  }

  /**
   * Lists all Oferta entities.
   *
   */
  public function indexAction() {
    $em = $this->getDoctrine()->getManager();

    if ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN')) {
      
      $entities = $em->getRepository('UnivaliMyCineManiaBundle:Oferta')->findAll();
      
    } elseif ($this->get('security.context')->isGranted('ROLE_CINEMA')) {

      $usuario = $this->getUser();
      
      $pessoa = $em->getRepository('UnivaliMyCineManiaBundle:Pessoa')->findByUsuario( $usuario );
      
      $cinema = $em->getRepository('UnivaliMyCineManiaBundle:Cinema')->findByPessoa( $pessoa );

      $entities = $em->getRepository('UnivaliMyCineManiaBundle:Oferta')->findByCinema($cinema);
    }

    return $this->render('UnivaliMyCineManiaBundle:Oferta:index.html.twig', array(
                'entities' => $entities,
    ));
  }

  /**
   * Creates a new Oferta entity.
   *
   */
  public function createAction(Request $request) {
    $entity = new Oferta();
    $form = $this->createForm(new OfertaType(), $entity);
    $form->bind($request);

    if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $em->persist($entity);
      $em->flush();

      return $this->redirect($this->generateUrl('oferta_show', array('id' => $entity->getId())));
    }

    return $this->render('UnivaliMyCineManiaBundle:Oferta:new.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
    ));
  }

  /**
   * Displays a form to create a new Oferta entity.
   *
   */
  public function newAction() {
    $entity = new Oferta();
    $form = $this->createForm(new OfertaType(), $entity);

    return $this->render('UnivaliMyCineManiaBundle:Oferta:new.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
    ));
  }

  /**
   * Finds and displays a Oferta entity.
   *
   */
  public function showAction($id) {
    $em = $this->getDoctrine()->getManager();

    $entity = $em->getRepository('UnivaliMyCineManiaBundle:Oferta')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find Oferta entity.');
    }

    $deleteForm = $this->createDeleteForm($id);

    return $this->render('UnivaliMyCineManiaBundle:Oferta:show.html.twig', array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),));
  }

  /**
   * Displays a form to edit an existing Oferta entity.
   *
   */
  public function editAction($id) {
    $em = $this->getDoctrine()->getManager();

    $entity = $em->getRepository('UnivaliMyCineManiaBundle:Oferta')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find Oferta entity.');
    }

    $editForm = $this->createForm(new OfertaType(), $entity);
    $deleteForm = $this->createDeleteForm($id);

    return $this->render('UnivaliMyCineManiaBundle:Oferta:edit.html.twig', array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
    ));
  }

  /**
   * Edits an existing Oferta entity.
   *
   */
  public function updateAction(Request $request, $id) {
    $em = $this->getDoctrine()->getManager();

    $entity = $em->getRepository('UnivaliMyCineManiaBundle:Oferta')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find Oferta entity.');
    }

    $deleteForm = $this->createDeleteForm($id);
    $editForm = $this->createForm(new OfertaType(), $entity);
    $editForm->bind($request);

    if ($editForm->isValid()) {
      $em->persist($entity);
      $em->flush();

      return $this->redirect($this->generateUrl('oferta_edit', array('id' => $id)));
    }

    return $this->render('UnivaliMyCineManiaBundle:Oferta:edit.html.twig', array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
    ));
  }

  /**
   * Deletes a Oferta entity.
   *
   */
  public function deleteAction(Request $request, $id) {
    $form = $this->createDeleteForm($id);
    $form->bind($request);

    if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $entity = $em->getRepository('UnivaliMyCineManiaBundle:Oferta')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Oferta entity.');
      }

      $em->remove($entity);
      $em->flush();
    }

    return $this->redirect($this->generateUrl('oferta'));
  }

  /**
   * Creates a form to delete a Oferta entity by id.
   *
   * @param mixed $id The entity id
   *
   * @return Symfony\Component\Form\Form The form
   */
  private function createDeleteForm($id) {
    return $this->createFormBuilder(array('id' => $id))
                    ->add('id', 'hidden')
                    ->getForm()
    ;
  }

  /**
   *
   */
  private function listOffers() {
    
  }

  public function exibirAction($id) {
    $em = $this->getDoctrine()->getManager();

    $entity = $em->getRepository('UnivaliMyCineManiaBundle:Oferta')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find Oferta entity.');
    }

    return $this->render('UnivaliMyCineManiaBundle:Oferta:exibir.html.twig', array(
                'entity' => $entity
    ));
  }

}
