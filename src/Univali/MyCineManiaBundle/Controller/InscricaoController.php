<?php

namespace Univali\MyCineManiaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Univali\MyCineManiaBundle\Entity\Cinefilo;
use Univali\MyCineManiaBundle\Form\CinefiloType;
use Univali\MyCineManiaBundle\Entity\Cinema;
use Univali\MyCineManiaBundle\Form\CinemaType;

/**
 * Cinefilo controller.
 *
 */
class InscricaoController extends Controller {

  /**
   *
   */
  public function cinefiloNewAction() {
    $entity = new Cinefilo();
    $form = $this->createForm(new CinefiloType(), $entity);

    return $this->render('UnivaliMyCineManiaBundle:Inscricao:cinefiloNew.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
    ));
  }

  /**
   *
   */
  public function cinefiloCreateAction(Request $request) {

    $em = $this->getDoctrine()->getManager();

    $cinefilo = new Cinefilo();
    $form = $this->createForm(new CinefiloType(), $cinefilo);
    $form->bind($request);

    $cinefilo->getPessoa()->setTipo('Fis');
            
    $emailExists = $em->getRepository('UnivaliMyCineManiaBundle:Usuario')->findByEmail($cinefilo->getPessoa()->getUsuario()->getEmail());
    if (sizeof($emailExists) > 0) {
      $form->addError(new \Symfony\Component\Form\FormError("Este e-mail já está sendo usado."));
    }

    $usuario = $cinefilo->getPessoa()->getUsuario();

    if ($form->isValid() && !$form->hasErrors()) {
      
      $em->persist($cinefilo);
      $em->flush();

      $userManager = $this->get('fos_user.user_manager');
      $user = $userManager->findUserByUsername( $usuario->getUsername() );
      $user->setPlainPassword( $usuario->getPassword() );
      $user->addRole('ROLE_CINEFILO');
      $userManager->updateUser($user);
      
      return $this->redirect($this->generateUrl('homepage'));
    }

    return $this->render('UnivaliMyCineManiaBundle:Inscricao:cinefiloNew.html.twig', array(
                'entity' => $cinefilo,
                'form' => $form->createView(),
    ));
  }

  /**
   *
   */
  public function cinemaNewAction() {
    $entity = new Cinema();
    $form = $this->createForm(new CinemaType(), $entity);

    return $this->render('UnivaliMyCineManiaBundle:Inscricao:cinemaNew.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
    ));
  }

  /**
   *
   */
  public function cinemaCreateAction(Request $request) {
    $em = $this->getDoctrine()->getManager();

    $cinema = new Cinema();
    $form = $this->createForm(new CinemaType(), $cinema);
    $form->bind($request);

    $cinema->getPessoa()->setTipo('Fis');
            
    $emailExists = $em->getRepository('UnivaliMyCineManiaBundle:Usuario')->findByEmail($cinema->getPessoa()->getUsuario()->getEmail());
    if (sizeof($emailExists) > 0) {
      $form->addError(new \Symfony\Component\Form\FormError("Este e-mail já está sendo usado."));
    }

    $userManager = $this->get('fos_user.user_manager');

    $usuario = $cinema->getPessoa()->getUsuario();


    if ($form->isValid() && !$form->hasErrors()) {
      
      $em->persist($cinema);
      $em->flush();

      $userManager = $this->get('fos_user.user_manager');
      $user = $userManager->findUserByUsername( $usuario->getUsername() );
      $user->setPlainPassword( $usuario->getPassword() );
      $user->setEnabled( true );
      $user->addRole('ROLE_CINEMA');
      $userManager->updateUser($user);
      
      return $this->redirect($this->generateUrl('homepage'));
    }
    
    return $this->render('UnivaliMyCineManiaBundle:Inscricao:cinemaNew.html.twig', array(
                'entity' => $cinema,
                'form' => $form->createView(),
    ));
  }

}
