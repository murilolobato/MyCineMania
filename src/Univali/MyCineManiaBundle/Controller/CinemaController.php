<?php

namespace Univali\MyCineManiaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Univali\MyCineManiaBundle\Entity\Cinema;
use Univali\MyCineManiaBundle\Form\CinemaType;

/**
 * Cinema controller.
 *
 */
class CinemaController extends Controller {

  /**
   * Lists all Cinema entities.
   *
   */
  public function indexAction() {
    $em = $this->getDoctrine()->getManager();

    $entities = $em->getRepository('UnivaliMyCineManiaBundle:Cinema')->findAll();

    return $this->render('UnivaliMyCineManiaBundle:Cinema:index.html.twig', array(
                'entities' => $entities,
    ));
  }

  /**
   * Creates a new Cinema entity.
   *
   */
  public function createAction(Request $request) {

    $em = $this->getDoctrine()->getManager();

    $cinema = new Cinema();
    $form = $this->createForm(new CinemaType(), $cinema);
    $form->bind($request);

    $cinema->getPessoa()->setTipo('Fis');

    $emailExists = $em->getRepository('UnivaliMyCineManiaBundle:Usuario')->findByEmail($cinema->getPessoa()->getUsuario()->getEmail());
    if (sizeof($emailExists) > 0) {
      $form->addError(new \Symfony\Component\Form\FormError("Este e-mail já está sendo usado."));
    }

    if ($form->isValid() && !$form->hasErrors()) {

      $em->persist($cinema);
      $em->flush();

      $usuario = $cinema->getPessoa()->getUsuario();

      $userManager = $this->get('fos_user.user_manager');
      $user = $userManager->findUserByUsername($usuario->getUsername());
      $user->setPlainPassword($usuario->getPassword());
      $user->setEnabled(true);
      $userManager->updateUser($user);

      return $this->redirect($this->generateUrl('cinema_show', array('id' => $cinema->getId())));
    }

    return $this->render('UnivaliMyCineManiaBundle:Cinema:new.html.twig', array(
                'entity' => $cinema,
                'form' => $form->createView(),
    ));
  }

  /**
   * Displays a form to create a new Cinema entity.
   *
   */
  public function newAction() {
    $entity = new Cinema();
    $form = $this->createForm(new CinemaType(), $entity);

    return $this->render('UnivaliMyCineManiaBundle:Cinema:new.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
    ));
  }

  /**
   * Finds and displays a Cinema entity.
   *
   */
  public function showAction($id) {
    $em = $this->getDoctrine()->getManager();

    $entity = $em->getRepository('UnivaliMyCineManiaBundle:Cinema')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find Cinema entity.');
    }

    $deleteForm = $this->createDeleteForm($id);

    return $this->render('UnivaliMyCineManiaBundle:Cinema:show.html.twig', array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),));
  }

  /**
   * Displays a form to edit an existing Cinema entity.
   *
   */
  public function editAction($id) {
    $em = $this->getDoctrine()->getManager();

    $entity = $em->getRepository('UnivaliMyCineManiaBundle:Cinema')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find Cinema entity.');
    }

    $editForm = $this->createForm(new CinemaType(), $entity);
    $deleteForm = $this->createDeleteForm($id);

    return $this->render('UnivaliMyCineManiaBundle:Cinema:edit.html.twig', array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
    ));
  }

  /**
   * Edits an existing Cinema entity.
   *
   */
  public function updateAction(Request $request, $id) {
    $em = $this->getDoctrine()->getManager();

    $cinema = $em->getRepository('UnivaliMyCineManiaBundle:Cinema')->find($id);

    if (!$cinema) {
      throw $this->createNotFoundException('Unable to find Cinema entity.');
    }

    $editForm = $this->createForm(new CinemaType(), $cinema);
    $editForm->bind($request);

    if ($editForm->isValid()) {

      $usuario = $cinema->getPessoa()->getUsuario();

      $emailExists = $em->getRepository('UnivaliMyCineManiaBundle:Usuario')->findByEmail($usuario->getEmail());
      if (sizeof($emailExists) > 0) {
        foreach ($emailExists as $usuarioExistente) {
          if ($usuario->getUsername() != $usuarioExistente->getUsername()) {
            $editForm->addError(new \Symfony\Component\Form\FormError("Este e-mail já está sendo usado."));
          }
        }
      }

      if ($editForm->hasErrors()) {
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('UnivaliMyCineManiaBundle:Cinema:edit.html.twig', array(
                    'entity' => $cinema,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
      } else {
        $em->persist($cinema);
        $em->flush();

        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($usuario->getUsername());
        $user->setPlainPassword($usuario->getPassword());
        $userManager->updateUser($user);
        
        return $this->redirect($this->generateUrl('cinema_edit', array('id' => $id)));
      }
    }

    return $this->render('UnivaliMyCineManiaBundle:Cinema:edit.html.twig', array(
                'entity' => $cinema,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
    ));
  }

  /**
   * Deletes a Cinema entity.
   *
   */
  public function deleteAction(Request $request, $id) {
    $form = $this->createDeleteForm($id);
    $form->bind($request);

    if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $entity = $em->getRepository('UnivaliMyCineManiaBundle:Cinema')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Cinema entity.');
      }

      $em->remove($entity);
      $em->flush();
    }

    return $this->redirect($this->generateUrl('cinema'));
  }

  /**
   * Creates a form to delete a Cinema entity by id.
   *
   * @param mixed $id The entity id
   *
   * @return Symfony\Component\Form\Form The form
   */
  private function createDeleteForm($id) {
    return $this->createFormBuilder(array('id' => $id))
                    ->add('id', 'hidden')
                    ->getForm()
    ;
  }

  /**
   *
   */
  public function exibirInformacoesPessoaisAction() {
    $em = $this->getDoctrine()->getManager();

    /* @var $user Univali\MyCineManiaBundle\Entity\User */
    $usuario = $this->get('security.context')->getToken()->getUser();

    $pessoa = $em->getRepository('UnivaliMyCineManiaBundle:Pessoa')->findByUsuario($usuario);

    $cinema = $em->getRepository('UnivaliMyCineManiaBundle:Cinema')->findByPessoa($pessoa[0]);

    if (!$cinema) {
      throw $this->createNotFoundException('Unable to find Cinema entity.');
    }

    return $this->render('UnivaliMyCineManiaBundle:Cinema:exibirInformacoesPessoais.html.twig', array('cinema' => $cinema[0]));
  }

}
