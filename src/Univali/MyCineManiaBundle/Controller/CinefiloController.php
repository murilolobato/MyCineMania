<?php

namespace Univali\MyCineManiaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Univali\MyCineManiaBundle\Entity\Cinefilo;
use Univali\MyCineManiaBundle\Form\CinefiloType;

/**
 * Cinefilo controller.
 *
 */
class CinefiloController extends Controller {

  /**
   * Lists all Cinefilo entities.
   *
   */
  public function indexAction() {
    $em = $this->getDoctrine()->getManager();

    $entities = $em->getRepository('UnivaliMyCineManiaBundle:Cinefilo')->findAll();

    return $this->render('UnivaliMyCineManiaBundle:Cinefilo:index.html.twig', array(
                'entities' => $entities,
    ));
  }

  /**
   * Creates a new Cinefilo entity.
   *
   */
  public function createAction(Request $request) {

    $em = $this->getDoctrine()->getManager();

    $cinefilo = new Cinefilo();
    $form = $this->createForm(new CinefiloType(), $cinefilo);
    $form->bind($request);

    $cinefilo->getPessoa()->setTipo('Fis');
            
    $emailExists = $em->getRepository('UnivaliMyCineManiaBundle:Usuario')->findByEmail($cinefilo->getPessoa()->getUsuario()->getEmail());
    if (sizeof($emailExists) > 0) {
      $form->addError(new \Symfony\Component\Form\FormError("Este e-mail já está sendo usado."));
    }

    $userManager = $this->get('fos_user.user_manager');


    if ($form->isValid() && !$form->hasErrors()) {
      
      $em->persist($cinefilo);
      $em->flush();
      
      $usuario = $cinefilo->getPessoa()->getUsuario();

      $userManager = $this->get('fos_user.user_manager');
      $user = $userManager->findUserByUsername( $usuario->getUsername() );
      $user->setPlainPassword( $usuario->getPassword() );
      $userManager->updateUser($user);

      return $this->redirect($this->generateUrl('cinefilo_show', array('id' => $cinefilo->getId())));
    }

    return $this->render('UnivaliMyCineManiaBundle:Cinefilo:new.html.twig', array(
                'entity' => $cinefilo,
                'form' => $form->createView(),
    ));
  }

  /**
   * Displays a form to create a new Cinefilo entity.
   *
   */
  public function newAction() {
    $entity = new Cinefilo();
    $form = $this->createForm(new CinefiloType(), $entity);

    return $this->render('UnivaliMyCineManiaBundle:Cinefilo:new.html.twig', array(
                'entity' => $entity,
                'form' => $form->createView(),
    ));
  }

  /**
   * Finds and displays a Cinefilo entity.
   *
   */
  public function showAction($id) {
    $em = $this->getDoctrine()->getManager();

    $entity = $em->getRepository('UnivaliMyCineManiaBundle:Cinefilo')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find Cinefilo entity.');
    }

    $deleteForm = $this->createDeleteForm($id);

    return $this->render('UnivaliMyCineManiaBundle:Cinefilo:show.html.twig', array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),));
  }

  /**
   * Displays a form to edit an existing Cinefilo entity.
   *
   */
  public function editAction($id) {
    $em = $this->getDoctrine()->getManager();

    $entity = $em->getRepository('UnivaliMyCineManiaBundle:Cinefilo')->find($id);

    if (!$entity) {
      throw $this->createNotFoundException('Unable to find Cinefilo entity.');
    }

    $editForm = $this->createForm(new CinefiloType(), $entity);
    $deleteForm = $this->createDeleteForm($id);

    return $this->render('UnivaliMyCineManiaBundle:Cinefilo:edit.html.twig', array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
    ));
  }

  /**
   * Edits an existing Cinefilo entity.
   *
   */
  public function updateAction(Request $request, $id) {
    $em = $this->getDoctrine()->getManager();

    $cinefilo = $em->getRepository('UnivaliMyCineManiaBundle:Cinefilo')->find($id);

    if (!$cinefilo) {
      throw $this->createNotFoundException('Unable to find Cinefilo entity.');
    }

    $editForm = $this->createForm(new CinefiloType(), $cinefilo);
    $editForm->bind($request);

    if ($editForm->isValid()) {

      $usuario = $cinefilo->getPessoa()->getUsuario();

      $emailExists = $em->getRepository('UnivaliMyCineManiaBundle:Usuario')->findByEmail($usuario->getEmail());
      if (sizeof($emailExists) > 0) {
        foreach ($emailExists as $usuarioExistente) {
          if ( $usuario->getUsername() != $usuarioExistente->getUsername() ) {
            $editForm->addError(new \Symfony\Component\Form\FormError("Este e-mail já está sendo usado."));
          }
        }
      }
      
      if ($editForm->hasErrors()) {
        $deleteForm = $this->createDeleteForm($id);
        
        return $this->render('UnivaliMyCineManiaBundle:Cinefilo:edit.html.twig', array(
                    'entity' => $cinefilo,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
      } else {
        $em->persist($cinefilo);
        $em->flush();
        
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByUsername($usuario->getUsername());
        $user->setPlainPassword($usuario->getPassword());
        $userManager->updateUser($user);
        
        return $this->redirect($this->generateUrl('cinefilo_edit', array('id' => $id)));
      }
    }

    return $this->render('UnivaliMyCineManiaBundle:Cinefilo:edit.html.twig', array(
                'entity' => $cinefilo,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
    ));
  }

  /**
   * Deletes a Cinefilo entity.
   *
   */
  public function deleteAction(Request $request, $id) {
    $form = $this->createDeleteForm($id);
    $form->bind($request);

    if ($form->isValid()) {
      $em = $this->getDoctrine()->getManager();
      $entity = $em->getRepository('UnivaliMyCineManiaBundle:Cinefilo')->find($id);

      if (!$entity) {
        throw $this->createNotFoundException('Unable to find Cinefilo entity.');
      }

      $em->remove($entity);
      $em->flush();
    }

    return $this->redirect($this->generateUrl('cinefilo'));
  }

  /**
   * Creates a form to delete a Cinefilo entity by id.
   *
   * @param mixed $id The entity id
   *
   * @return Symfony\Component\Form\Form The form
   */
  private function createDeleteForm($id) {
    return $this->createFormBuilder(array('id' => $id))
                    ->add('id', 'hidden')
                    ->getForm()
    ;
  }

  /**
   *
   */
  public function emitirBoletoAction() {
    $em = $this->getDoctrine()->getManager();

    /* @var $user Univali\MyCineManiaBundle\Entity\User */
    $usuario = $this->get('security.context')->getToken()->getUser();

    $pessoa = $em->getRepository('UnivaliMyCineManiaBundle:Pessoa')->findByUsuario($usuario);

    $cinefilo = $em->getRepository('UnivaliMyCineManiaBundle:Cinefilo')->findByPessoa($pessoa);

    if (!$cinefilo) {
      throw $this->createNotFoundException('Unable to find Cinefilo entity.');
    }

    return $this->render('UnivaliMyCineManiaBundle:Cinefilo:emitirBoleto.html.twig', array('cinefilo' => $cinefilo[0], 'pessoa' => $pessoa[0]));
  }

  /**
   *
   */
  public function exibirInformacoesPessoaisAction() {
    $em = $this->getDoctrine()->getManager();

    /* @var $user Univali\MyCineManiaBundle\Entity\User */
    $usuario = $this->get('security.context')->getToken()->getUser();

    $pessoa = $em->getRepository('UnivaliMyCineManiaBundle:Pessoa')->findByUsuario($usuario);

    $cinefilo = $em->getRepository('UnivaliMyCineManiaBundle:Cinefilo')->findByPessoa($pessoa);

    if (!$cinefilo) {
      throw $this->createNotFoundException('Unable to find Cinefilo entity.');
    }

    return $this->render('UnivaliMyCineManiaBundle:Cinefilo:exibirInformacoesPessoais.html.twig', array('cinefilo' => $cinefilo[0], 'pessoa' => $pessoa[0]));
  }

  public function aprovarAction($id) {
    $em = $this->getDoctrine()->getManager();

    $cinefilo = $em->getRepository('UnivaliMyCineManiaBundle:Cinefilo')->find($id);

    $movimentacao = new \Univali\MyCineManiaBundle\Entity\Movimentacao();

    $movimentacao->setCinefilo($cinefilo);

    $movimentacao->setTipo('Inc');

    $movimentacao->setData(new \DateTime(date("Y-m-d")));

    $movimentacao->setDataCadastro(new \DateTime(date("Y-m-d")));

    $movimentacao->setDataValidade(new \DateTime(date("Y-m-d", strtotime('+1 year'))));

    $em->persist($movimentacao);

    $em->flush();

    $userManager = $this->get('fos_user.user_manager');
    $user = $userManager->findUserByUsername( $cinefilo->getPessoa()->getUsuario()->getUsername() );
    $user->setEnabled(true);
    $userManager->updateUser($user);
    
    return $this->redirect($this->generateUrl('cinefilo'));
  }

  public function naoAprovarAction($id) {
    $em = $this->getDoctrine()->getManager();

    $cinefilo = $em->getRepository('UnivaliMyCineManiaBundle:Cinefilo')->findById($id, null, 1);

    $movimentacao = new \Univali\MyCineManiaBundle\Entity\Movimentacao();

    $movimentacao->setCinefilo($cinefilo[0]);

    $movimentacao->setTipo('Exc');

    $movimentacao->setData(new \DateTime(date("Y-m-d")));

    $movimentacao->setDataCadastro(new \DateTime(date("Y-m-d")));

    $movimentacao->setDataValidade(null);

    $em->persist($movimentacao);

    $em->flush();

    $userManager = $this->get('fos_user.user_manager');
    $user = $userManager->findUserByUsername( $cinefilo->getPessoa()->getUsuario()->getUsername() );
    $user->setEnabled(false);
    $userManager->updateUser($user);

    return $this->redirect($this->generateUrl('cinefilo'));
  }

}
