<?php

namespace Univali\MyCineManiaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VoucherType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dataEmissao')
            ->add('situacao')
            ->add('oferta')
            ->add('cinefilo')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Univali\MyCineManiaBundle\Entity\Voucher'
        ));
    }

    public function getName()
    {
        return 'univali_mycinemaniabundle_vouchertype';
    }
}
