<?php

namespace Univali\MyCineManiaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class OfertaType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder
            ->add('dataInicio', 'date', array(
                'label' => 'Data de início', 
                'widget' => 'single_text'
            ))
            ->add('dataTermino', 'date', array(
                'label' => 'Data de término', 
                'widget' => 'single_text'
            ))
            ->add('descricao')
            ->add('regra')
            ->add('titulo')
            ->add('cinema')
    ;
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver) {
    $resolver->setDefaults(array(
        'data_class' => 'Univali\MyCineManiaBundle\Entity\Oferta'
    ));
  }

  public function getName() {
    return 'univali_mycinemaniabundle_ofertatype';
  }

}
