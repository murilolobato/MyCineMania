<?php

namespace Univali\MyCineManiaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ValidarVoucherType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('codigo', null, array('mapped' => false, 'label' => 'Código'))
        ;
    }

    public function getName()
    {
        return 'univali_mycinemaniabundle_validarvouchertype';
    }
}
