<?php

namespace Univali\MyCineManiaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PessoaType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder
            ->add('documento')
            ->add('nome')
            ->add('usuario', new UsuarioType())
    ;
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver) {
    $resolver->setDefaults(array(
        'data_class' => 'Univali\MyCineManiaBundle\Entity\Pessoa'
    ));
  }

  public function getName() {
    return 'univali_mycinemaniabundle_pessoatype';
  }

}
