<?php

namespace Univali\MyCineManiaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UsuarioType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder
            ->add('username', null, array('label' => 'Nome de usuário'))
            ->add('password', 'password', array('label' => 'Senha'))
            ->add('email', null, array('label' => 'Email'))
    ;
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver) {
    $resolver->setDefaults(array(
        'data_class' => 'Univali\MyCineManiaBundle\Entity\Usuario'
    ));
  }

  public function getName() {
    return 'univali_mycinemaniabundle_usuariotype';
  }

}
