<?php

namespace Univali\MyCineManiaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CinefiloType extends AbstractType {

  public function buildForm(FormBuilderInterface $builder, array $options) {
    $builder
            ->add('cpf')
            ->add('pessoa', new PessoaType())
    ;
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver) {
    $resolver->setDefaults(array(
        'data_class' => 'Univali\MyCineManiaBundle\Entity\Cinefilo'
    ));
  }

  public function getName() {
    return 'univali_mycinemaniabundle_cinefilotype';
  }

}
